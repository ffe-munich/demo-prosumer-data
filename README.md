<img src="https://www.ffe.de/wp-content/uploads/2020/05/InDEED_Logo_pos_Rand-1.png" height="150">
<img src="https://upload.wikimedia.org/wikipedia/commons/9/99/Gefoerdert_BMWE_Logo.png" width="175" height="150">

<!-- ABOUT THE PROJECT -->

## About The Project

This repository originates in a research project called [InDEED](https://www.ffe.de/projekte/indeed/) (funding code: 03EI6026A) which is a project funded by the
German Federal Ministry for Economic Affairs and Climate Action. With the aim of not developing a proprietary data platform, but rather relying on the strength of an "ecosystem," this project involves providing generation and consumption data. These data should serve as a foundation for platform simulation and the development of new use cases and business models for external actors as well.

# Demo Prosumer Data

The data was captured during 2022 in a townhouse with a two-person household (construction in 2020). Missing values (due to outages in the measurement) have been interpolated by building the averaging the values before and after each gap.

This repository contains a single dataset, i. e. timeseries data with two values:
- household load in watts
- load of a rooftop PV plant in percent of installed power

Together, both values represent an example prosumer household. The PV load profile was derived from weather data for a small city in the metropolitan region around Munich, Bavaria. The household load was derived from real measurements at an anonymus household in the same area. The data may be used as a foundation for imitating different prosumers in platform simulations.

Below is a plot of monthly aggregated household load and energy produced by a PV plant with an installed capacity of 8 kWp.

![data visualization](data_visualization.png)

<!-- LICENSE -->

## License

### BSD-3-Clause License with Common Clause

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the InDEED project nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

4. This software may be used for non-commercial purposes only. For the purposes of this license, "non-commercial use" means use solely for personal or research purposes or use by organizations that are non-profit, non-commercial, or educational in nature. Any use of the software for a commercial purpose is prohibited without the prior written consent of the copyright holder.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

<!-- CONTACT -->

## Contact

Joachim Ferstl - jferstl@ffe.de / info@ffe.de
